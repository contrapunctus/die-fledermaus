A project to create a transcript of this (IMHO) excellent production of Die Fledermaus, and eventually make that into subtitles -

https://www.youtube.com/watch?v=e4iuAVDvJE4

Help is appreciated. Doubts and omissions are marked with "(?)" and "???" respectively.

If anyone knows the name of the translator, or the team behind this production, please let me know.
